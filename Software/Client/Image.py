from Path import *

from svgpathtools import svg2paths2
from svgpathtools import Path, Line, QuadraticBezier, CubicBezier, Arc, wsvg
import struct
import math
import socket
import time
import binascii

class Image:
	viewBox=[]
	count_paths=0
	pathlist=[]

	def __init__(self,path_image):
		self.metapaths,self.paths,self.svg_attributes=svg2paths2(path_image)
		self.viewBox=self.svg_attributes.get('viewBox',"None")
		self.viewBox=self.viewBox.split(' ')
		self.viewBox= float(self.viewBox[2])-float(self.viewBox[0]),float(self.viewBox[3])-float(self.viewBox[1]) #ancho y alto
		for i in range(0,len(self.paths)):
			fill=(self.paths[i].get('fill', None))
			style=(self.paths[i].get('style', None))
			if style != None:
				style = style.split('#')
				#print(style[1])
				style=style[1].split(';')
				style=style[0]
				fill=style
				#print(style,type(style))
			if(fill=='none' or fill=='None'):
				fill=None

			d=(self.paths[i].get('d', None))
			d=d.replace('M',' M ')
			d=d.replace('m',' m ')
			d=d.replace('L',' L ')
			d=d.replace('l',' l ')
			d=d.replace('C',' C ')
			d=d.replace('c',' c ')
			d=d.replace('Q',' Q ')
			d=d.replace('q',' q ')
			d=d.replace('H',' H ')
			d=d.replace('h',' h ')
			d=d.replace('V',' V ')
			d=d.replace('v',' v ')
			d=d.replace('Z',' Z ')
			d=d.replace('z',' z ')
			d=d.replace(',',' ')
			d=d.replace('-',' -')
			d=d.replace('    ',' ')			
			d=d.replace('   ',' ')
			d=d.replace('  ',' ')
			d=d.replace("["+"'","")
			d=d.replace("'"+"]","")
			d=d.split(' ')
			#print(fill)
			#print(type(fill))
			path=Paath(d,fill,style)

			self.pathlist.append(path)


	def getPathList(self):
		return self.pathlist

	def Print(self):
		print('Numero de paths',len(self.paths))
		print (self.paths)

	def getViewPort(self):
		return self.viewBox

