Client.py code has been separated for better understanding

An image is composed by a number of paths, each path by a list of objects. The aim is translate an image into a list of messages which the robot could understand.

Image.py calls svgpathtools and separates all information in the image and creates the corresponding paths. (An image is composed by a list of paths)

Path.py decomposed the information asociated (fill and d'string), and creates a list of object according to d'= property.

Objects.py has all the basic objects in the d= property of a path element (see SVG). Such us command lineto,cubicbezier,...

Message.py manage all the message information. Create a list of messages and translates the information in the basic elements of the path into messages to send via socket.
