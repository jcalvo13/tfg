import numpy

class Point:
	def __init__(self,x=0,y=0):
		self.x=x
		self.y=y

	def Print(self):
		print('Punto: ',self.x,self.y)


class MoveAbs:
	def __init__(self,x1=0,y1=0):
		self.fin=Point(x1,y1)
		self.ymin=None
		self.ymax=None
	def Print(self):
		print('Move abs de coordenadas: ',self.fin.x,self.fin.y)
	def Solve(self,y):
		return None


class MoveRel:
	def __init__(self,x1=0,y1=0):

		self.fin=Point(x1,y1)
		self.ymin=None
		self.ymax=None
	def Print(self):
		print('Move rel de coordenadas: ',self.fin.x,self.fin.y)
	def Solve(self,y):
		return None

class LineAbs:
	def __init__(self,x1=0,y1=0,x2=0,y2=0):
		self.ini=Point(x1,y1)
		self.fin=Point(x2,y2)
		self.getMinMax()
	def Print(self):
		print('Linea abs de coordenadas: ',self.ini.x,self.ini.y,self.fin.x,self.fin.y)
	def Solve(self,y):

		if (y>=self.ini.y and y<=self.fin.y) or (y<=self.ini.y and y>=self.fin.y):
			sol=[]

			if(self.fin.x-self.ini.x)==0:
				sol.append(self.ini.x)
				return sol


			elif(self.fin.y-self.ini.y)==0:
				return None

			else:
				
				m=(self.fin.y-self.ini.y)/(self.fin.x-self.ini.x)
				n=self.ini.y-m*self.ini.x
				x=(y-n)/m
				sol.append(x)
				return sol		
	def getMinMax(self): #Busco el minimo y max en eje Y
		
		if(self.ini.y<self.fin.y):
			self.ymin=self.ini.y
			self.ymax=self.fin.y
		else:
			self.ymin=self.fin.y
			self.ymax=self.ini.y



class LineRel:
	def __init__(self,x1=0,y1=0,x2=0,y2=0):
		
		self.ini=Point(x1,y1)
		self.fin=Point(x2,y2)
		self.getMinMax()
	
	def Print(self):
		print('Linea Rel de Coordenadas:', self.ini.x,self.ini.y,self.ini.x+self.fin.x,self.ini.y+self.fin.y)
	
	def Solve(self,y):
		finalx=self.ini.x+self.fin.x
		finaly=self.ini.y+self.fin.y
		sol=[]

		if (y>=self.ini.y and y<=finaly) or (y<=self.ini.y and y>=finaly):
			if(finalx-self.ini.x)==0:
				sol.append(self.ini.x)
				return sol


			elif(finaly-self.ini.y)==0:
				return None

			else:
				m=(finaly-self.ini.y)/(finalx-self.ini.x)
				n=self.ini.y-m*self.ini.x
				x=(y-n)/m
				sol.append(x)
				return sol

	def getMinMax(self): #Busco el minimo y max en eje Y
		fin=self.ini.y+self.fin.y
		if(self.ini.y<fin):
			self.ymin=self.ini.y
			self.ymax=fin
		else:
			self.ymin=fin
			self.ymax=self.ini.y


class CuaAbs:
	def __init__(self,x1=0,y1=0,x2=0,y2=0,x3=0,y3=0):

		self.ini=Point(x1,y1)
		self.c1=Point(x2,y2)
		self.fin=Point(x3,y3)
		self.getMinMax()		

	def Print(self):
		print('CuaAbs de Coordenadas:', self.ini.x,self.ini.y,self.c1.x,self.c1.y,self.fin.x,self.fin.y)
	
	def Solve(self,y): #despejamos t tras sustituir y, y hallamos x


		if(y>=self.ymin and y<=self.ymax):
			a=(self.ini.y-2*self.c1.y+self.fin.y)
			b=2*self.c1.y-2*self.ini.y
			c=self.ini.y-y
			x=[a,b,c]
			#print(x)
			sol=[]
			t=numpy.roots(x)
			#print(t)
			for i in range(0,len(t)):
				if (t[i]>=0 and t[i]<=1):
					sol.append(float(((1-t[i])**2)*self.ini.x+2*t[i]*(1-t[i])*self.c1.x+t[i]*t[i]*self.fin.x))

			return sol	

	def getMinMax(self): #Busco el minimo y max en eje Y
		num_puntos=40
		dy=1/num_puntos

		y_min=self.ini.y
		y_max=self.fin.y
		for i in range(0,num_puntos+1):
			t=i*dy
			y1=((1-t)**2)*self.ini.y+2*t*(1-t)*self.c1.y+t*t*self.fin.y
			if(y1>y_max):
				y_max=y1
			elif(y1<y_min):
				y_min=y1
		self.ymax=y_max
		self.ymin=y_min



class CuaRel:
	def __init__(self,x1=0,y1=0,x2=0,y2=0,x3=0,y3=0):

		self.ini=Point(x1,y1)
		self.c1=Point(x2,y2)
		self.fin=Point(x3,y3)
		self.getMinMax()

	def Print(self):
		print('CuaRel de Coordenadas:', self.ini.x,self.ini.y,self.ini.x+self.c1.x,self.ini.y+self.c1.y,self.ini.x+self.fin.x,self.ini.y+self.fin.y)	
	def Solve(self,y): #despejamos t tras sustituir y, y hallamos x
		c1x=self.ini.x+self.c1.x
		c1y=self.ini.y+self.c1.y
		finx=self.ini.x+self.fin.x
		finy=self.ini.y+self.fin.y
		if(y>=self.ymin and y<=self.ymax):
			a=(self.ini.y-2*c1y+finy)
			b=2*c1y-2*self.ini.y
			c=self.ini.y-y
			x=[a,b,c]
			#print(x)
			sol=[]
			t=numpy.roots(x)
			#print(t)
			for i in range(0,len(t)):
				if (t[i]>=0 and t[i]<=1):
					sol.append(float(((1-t[i])**2)*self.ini.x+2*t[i]*(1-t[i])*c1x+t[i]*t[i]*finx))

			return sol

#se puede realizar la funcion solve solo si y está en el intervalo, con ymin e ymax

	def getMinMax(self): 
		c1x=self.ini.x+self.c1.x
		c1y=self.ini.y+self.c1.y
		finx=self.ini.x+self.fin.x
		finy=self.ini.y+self.fin.y
		#utilizamos c1x,c1y,finx,finy (coordenadas absolutas)

		num_puntos=40
		dy=1/num_puntos

		y_min=self.ini.y
		y_max=finy
		for i in range(0,num_puntos+1):
			t=i*dy
			y1=((1-t)**2)*self.ini.y+2*t*(1-t)*c1y+t*t*finy
			if(y1>y_max):
				y_max=y1
			elif(y1<y_min):
				y_min=y1
		self.ymax=y_max
		self.ymin=y_min




class CubAbs:
	def __init__(self,x1=0,y1=0,x2=0,y2=0,x3=0,y3=0,x4=0,y4=0):

		self.ini=Point(x1,y1)
		self.c1=Point(x2,y2)
		self.c2=Point(x3,y3)
		self.fin=Point(x4,y4)
		self.getMinMax()

	def Print(self):
		print('CubAbs de Coordenadas:', self.ini.x,self.ini.y,self.c1.x,self.c1.y,self.c2.x,self.c2.y,self.fin.x,self.fin.y)	

	def Solve(self,y):

		if(y>=self.ymin and y<=self.ymax):   #Si esta en el intervalo de y, calculamos los puntos de corte
			a=-self.ini.y+3*self.c1.y-3*self.c2.y+self.fin.y
			b=3*self.ini.y-6*self.c1.y+3*self.c2.y
			c=-3*self.ini.y+3*self.c1.y
			d=self.ini.y-y
		
			x=[a,b,c,d]
			#print(x)
			sol=[]
			t=numpy.roots(x)
			#print(t)
			#no considerar t imaginarias

			t = t.real[abs(t.imag)<1e-5] # where I chose 1-e5 as a threshold
			#print(t)

			for i in range(0,len(t)):  #Agrupamos las soluciones en una lista
				if (t[i]>=0 and t[i]<=1):
					sol.append(float(self.ini.x*(1-t[i])**3+3*self.c1.x*t[i]*(1-t[i])**2+3*self.c2.x*t[i]*t[i]*(1-t[i])+self.fin.x*t[i]**3))
									
			return sol

	def getMinMax(self): 

		num_puntos=40
		dy=1/num_puntos

		y_min=self.ini.y
		y_max=self.fin.y
		for i in range(0,num_puntos+1):
			t=i*dy
			y1=self.ini.y*(1-t)**3+3*self.c1.y*t*(1-t)**2+3*self.c2.y*t*t*(1-t)+self.fin.y*t**3	
			if(y1>y_max):
				y_max=y1
			elif(y1<y_min):
				y_min=y1
		self.ymax=y_max
		self.ymin=y_min



class CubRel:
	def __init__(self,x1=0,y1=0,x2=0,y2=0,x3=0,y3=0,x4=0,y4=0):

		self.ini=Point(x1,y1)
		self.c1=Point(x2,y2)
		self.c2=Point(x3,y3)
		self.fin=Point(x4,y4)
		self.getMinMax()

	def Print(self):
		print('Cubrel de Coordenadas:', self.ini.x,self.ini.y,self.ini.x+self.c1.x,self.ini.y+self.c1.y,self.ini.x+self.c2.x,self.ini.y+self.c2.y,self.ini.x+self.fin.x,self.ini.y+self.fin.y)
		

	def Solve(self,y):
		c1x=self.ini.x+self.c1.x
		c1y=self.ini.y+self.c1.y
		c2x=self.ini.x+self.c2.x
		c2y=self.ini.y+self.c2.y		
		finx=self.ini.x+self.fin.x
		finy=self.ini.y+self.fin.y


		if(y>=self.ymin and y<=self.ymax):
			a=-self.ini.y+3*c1y-3*c2y+finy
			b=3*self.ini.y-6*c1y+3*c2y
			c=-3*self.ini.y+3*c1y
			d=self.ini.y-y
	
			x=[a,b,c,d]
			#print(x)
			sol=[]
			t=numpy.roots(x)
			#print(t)

			#no considerar t imaginarias

			t = t.real[abs(t.imag)<1e-5] # where I chose 1-e5 as a threshold
			#print(t)

			for i in range(0,len(t)):
				if (t[i]>=0 and t[i]<=1):
					sol.append(float(self.ini.x*(1-t[i])**3+3*c1x*t[i]*(1-t[i])**2+3*c2x*t[i]*t[i]*(1-t[i])+finx*t[i]**3))
									
			return sol
	def getMinMax(self):
		c1x=self.ini.x+self.c1.x
		c1y=self.ini.y+self.c1.y
		c2x=self.ini.x+self.c2.x
		c2y=self.ini.y+self.c2.y		
		finx=self.ini.x+self.fin.x
		finy=self.ini.y+self.fin.y
		num_puntos=40
		dy=1/num_puntos

		y_min=self.ini.y
		y_max=finy
		for i in range(0,num_puntos+1):
			t=i*dy
			y1=self.ini.y*(1-t)**3+3*c1y*t*(1-t)**2+3*c2y*t*t*(1-t)+finy*t**3	
			if(y1>y_max):
				y_max=y1
			elif(y1<y_min):
				y_min=y1
		self.ymax=y_max
		self.ymin=y_min




class ClosePath:
	def __init__(self):
		print('Fin de Path')
	def Print(self):
		print('Fin del Path')

#Añadir getMinMax
class VerAbs:
	def __init__(self,x1=0,y1=0,x2=0,y2=0):
		self.ini=Point(x1,y1)
		self.fin=Point(x2,y2)
	def Print(self):
		print('Linea vertical abs de coordenadas: ',self.ini.x,self.ini.y,self.fin.x,self.fin.y)
	def Solve(self,y):

		if (y>=self.ini.y and y<=self.fin.y) or (y<=self.ini.y and y>=self.fin.y):
			sol=[]

			sol.append(self.ini.x)
			return sol



class VerRel:
	def __init__(self,x1=0,y1=0,x2=0,y2=0):
		
		self.ini=Point(x1,y1)
		self.fin=Point(x2,y2)
	
	def Print(self):
		print('Linea vertical rel de Coordenadas:', self.ini.x,self.ini.y,self.ini.x+self.fin.x,self.ini.y+self.fin.y)
	
	def Solve(self,y):
		finalx=self.ini.x+self.fin.x
		finaly=self.ini.y+self.fin.y
		sol=[]

		if (y>=self.ini.y and y<=finaly) or (y<=self.ini.y and y>=finaly):
				sol.append(self.ini.x)
				return sol




class HorAbs:
	def __init__(self,x1=0,y1=0,x2=0,y2=0):
		self.ini=Point(x1,y1)
		self.fin=Point(x2,y2)
	def Print(self):
		print('Linea horizontal abs de coordenadas: ',self.ini.x,self.ini.y,self.fin.x,self.fin.y)
	def Solve(self,y):

		if (y>=self.ini.y and y<=self.fin.y) or (y<=self.ini.y and y>=self.fin.y):

				return None
			


class HorRel:
	def __init__(self,x1=0,y1=0,x2=0,y2=0):
		
		self.ini=Point(x1,y1)
		self.fin=Point(x2,y2)
	
	def Print(self):
		print('Linea horizontal Rel de Coordenadas:', self.ini.x,self.ini.y,self.ini.x+self.fin.x,self.ini.y+self.fin.y)
	
	def Solve(self,y):
		finalx=self.ini.x+self.fin.x
		finaly=self.ini.y+self.fin.y
		sol=[]

		if (y>=self.ini.y and y<=finaly) or (y<=self.ini.y and y>=finaly):

			return None
