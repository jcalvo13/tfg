from Objects import *

class Paath:
	elementList=[]
	def __init__(self,d=0,fill=None,style=None):
		self.fill=fill		#path colour
		self.d=d	#d'string'
		self.current_point=Point(0.0,0.0)
		self.current_element=None
		self.elementList=[]
		self.i=1
		self.Object=None
		self.ymin=None
		self.ymax=None
		
		self.path2elements()
   #saber mejor como funciona el __main__

	def path2elements(self):	#el path es capaz de descomponer el values en sus elementos
		while self.i<(len(self.d)-1):
			if self.d[self.i]=="M":
				self.current_element=1
				self.i=self.i+1
			elif self.d[self.i]=="m":
				self.current_element=2
				self.i=self.i+1
			elif self.d[self.i]=="L":
				self.current_element=3
				self.i=self.i+1
			elif self.d[self.i]=="l":
				self.current_element=4
				self.i=self.i+1
			elif self.d[self.i]=="Q":
				self.current_element=5
				self.i=self.i+1
			elif self.d[self.i]=="q":
				self.current_element=6
				self.i=self.i+1
			elif self.d[self.i]=="C":
				self.current_element=7
				self.i=self.i+1
			elif self.d[self.i]=="c":
				self.current_element=8
				self.i=self.i+1
			elif self.d[self.i]=="V":
				self.current_element=9
				self.i=self.i+1
			elif self.d[self.i]=="v":
				self.current_element=10
				self.i=self.i+1
			elif self.d[self.i]=="H":
				self.current_element=11
				self.i=self.i+1
			elif self.d[self.i]=="h":
				self.current_element=12
				self.i=self.i+1
			elif self.d[self.i]=="Z":
				self.current_element=0
				self.i=self.i+1
			elif self.d[self.i]=="z":
				self.current_element=0
				self.i=self.i+1
			else:
				if(self.current_element==0):     #No llega a entrar aquí nunca, mirar más adelante
					if(self.d[self.i])==" ":
						self.Object=ClosePath()
						self.i=self.i+1
				
				elif(self.current_element==1):

					self.d[self.i]=float(self.d[self.i])
					self.d[self.i+1]=float(self.d[self.i+1])
					self.Object=MoveAbs(self.d[self.i],self.d[self.i+1])
					#Actualizamos Current point
					self.current_point.x=self.d[self.i]
					self.current_point.y=self.d[self.i+1]
					self.i=self.i+2

				elif(self.current_element==2):
					self.d[self.i]=float(self.d[self.i])
					self.d[self.i+1]=float(self.d[self.i+1])
					self.Object=MoveRel(self.d[self.i],self.d[self.i+1])
					#Actualizamos Current point
					self.current_point.x=self.current_point.x+self.d[self.i]
					self.current_point.y=self.current_point.y+self.d[self.i+1]
					self.i=self.i+2
				elif(self.current_element==3):
					self.d[self.i]=float(self.d[self.i])
					self.d[self.i+1]=float(self.d[self.i+1])					
					self.Object=LineAbs(self.current_point.x,self.current_point.y,self.d[self.i],self.d[self.i+1])
					#Actualizamos Current point
					self.current_point.x=self.d[self.i]
					self.current_point.y=self.d[self.i+1]
					self.i=self.i+2
				elif(self.current_element==4):
					self.d[self.i]=float(self.d[self.i])
					self.d[self.i+1]=float(self.d[self.i+1])
					self.Object=LineRel(self.current_point.x,self.current_point.y,self.d[self.i],self.d[self.i+1])
					#Actualizamos Current point
					self.current_point.x=self.current_point.x+self.d[self.i]
					self.current_point.y=self.current_point.y+self.d[self.i+1]
					self.i=self.i+2
				elif(self.current_element==9):
					self.d[self.i]=float(self.d[self.i])
					self.Object=VerAbs(self.current_point.x,self.current_point.y,self.current_point.x,self.d[self.i])
					#Actualizamos Current point
					self.current_point.y=self.d[self.i]
					self.i=self.i+1
				elif(self.current_element==10):
					self.d[self.i]=float(self.d[self.i])
					self.Object=VerRel(self.current_point.x,self.current_point.y,self.current_point.x,self.d[self.i])
					#Actualizamos Current point
					self.current_point.y=self.current_point.y+self.d[self.i]
					self.i=self.i+1
				elif(self.current_element==11):
					self.d[self.i]=float(self.d[self.i])
					self.Object=HorAbs(self.current_point.x,self.current_point.y,self.d[self.i],self.current_point.y)
					#Actualizamos Current point
					self.current_point.x=self.d[self.i]
					self.i=self.i+1
				elif(self.current_element==12):
					self.d[self.i]=float(self.d[self.i])
					self.Object=HorRel(self.current_point.x,self.current_point.y,self.d[self.i],self.current_point.y)
					#Actualizamos Current point
					self.current_point.x=self.current_point.x+self.d[self.i]
					self.i=self.i+1
				elif(self.current_element==5):
					self.d[self.i]=float(self.d[self.i])
					self.d[self.i+1]=float(self.d[self.i+1])
					self.d[self.i+2]=float(self.d[self.i+2])
					self.d[self.i+3]=float(self.d[self.i+3])					
					self.Object=QuaAbs(self.current_point.x,self.current_point.y,self.d[self.i],self.d[self.i+1],self.d[self.i+2],self.d[self.i+3])
					#Actualizamos Current point
					self.current_point.x=self.d[self.i+2]
					self.current_point.y=self.d[self.i+3]
					self.i=self.i+4
				elif(self.current_element==6):
					self.d[self.i]=float(self.d[self.i])
					self.d[self.i+1]=float(self.d[self.i+1])
					self.d[self.i+2]=float(self.d[self.i+2])
					self.d[self.i+3]=float(self.d[self.i+3])					
					self.Object=QuaRel(self.current_point.x,self.current_point.y,self.d[self.i],self.d[self.i+1],self.d[self.i+2],self.d[self.i+3])
					#Actualizamos Current point
					self.current_point.x=self.current_point.x+self.d[self.i+2]
					self.current_point.y=self.current_point.y+self.d[self.i+3]
					self.i=self.i+4
				elif(self.current_element==7):
					self.d[self.i]=float(self.d[self.i])
					self.d[self.i+1]=float(self.d[self.i+1])
					self.d[self.i+2]=float(self.d[self.i+2])
					self.d[self.i+3]=float(self.d[self.i+3])
					self.d[self.i+4]=float(self.d[self.i+4])
					self.d[self.i+5]=float(self.d[self.i+5])										
					self.Object=CubAbs(self.current_point.x,self.current_point.y,self.d[self.i],self.d[self.i+1],self.d[self.i+2],self.d[self.i+3],self.d[self.i+4],self.d[self.i+5])
					#Actualizamos Current point
					self.current_point.x=self.d[self.i+4]
					self.current_point.y=self.d[self.i+5]
					self.i=self.i+6
				elif(self.current_element==8):
					self.d[self.i]=float(self.d[self.i])
					self.d[self.i+1]=float(self.d[self.i+1])
					self.d[self.i+2]=float(self.d[self.i+2])
					self.d[self.i+3]=float(self.d[self.i+3])
					self.d[self.i+4]=float(self.d[self.i+4])
					self.d[self.i+5]=float(self.d[self.i+5])						
					self.Object=CubRel(self.current_point.x,self.current_point.y,self.d[self.i],self.d[self.i+1],self.d[self.i+2],self.d[self.i+3],self.d[self.i+4],self.d[self.i+5])
					#Actualizamos Current point
					self.current_point.x=self.current_point.x+self.d[self.i+4]
					self.current_point.y=self.current_point.y+self.d[self.i+5]
					self.i=self.i+6

				self.elementList.append(self.Object)
				#print(type(self.Object))

				self.getMinMax()
		#print('Min/Max:',self.ymin,self.ymax)

	def printPath(self):
		for i in range(0,len(self.elementList)):
			print('Elemento:',i)
			self.elementList[i].Print()

	def color2gray(self):
		if(type(self.fill) is str):
			
			if(len(self.fill)==7): #cadena de 7 caracteres del tipo #00ffee
				r=self.fill[1]+self.fill[2]
				g=self.fill[3]+self.fill[4]
				b=self.fill[5]+self.fill[6]
				
				r = int(r, 16)
				g = int(g, 16)
				b = int(b, 16)
				#return float(0.33*r+0.33*b+0.33*g)
				return float(0.33*r+0.33*g+0.33*b)
				#return float(0.3*r+0.66*g+0.11*b) #Ecuación tomada como conversion entre rgb y escala de grises
	def color2rgb(self):
		#if(len(self.fill==7)):
		
		if(type(self.fill) is str):
			
			r=self.fill[1]+self.fill[2]
			g=self.fill[3]+self.fill[4]
			b=self.fill[5]+self.fill[6]
			#print(r,g,b)
			r = int(r, 16)
			g = int(g, 16)
			b = int(b, 16)
			
			return r,g,b


	def color2CMYK(self):

		r,g,b=self.color2rgb()
		r=r/255
		g=g/255
		b=b/255
		C=0
		M=0
		Y=0
		#ecuaciones rgb2cmyk
		K=1-max(r,g,b)
		if(K<1):
			C=(1-r-K)/(1-K)
			M=(1-g-K)/(1-K)
			Y=(1-b-K)/(1-K)
		return K,C,M,Y


	def append(self,Object):
		self.elementList.append(Object)

	def getMinMax(self):		
			if(type(self.Object)!=type(MoveAbs()) and type(self.Object)!=type(MoveRel())):
				#print(type(self.Object.ymin),type(self.Object.ymax))

				if(self.ymin == None ):
					
					self.ymin=self.Object.ymin
					self.ymax=self.Object.ymax
					#print('Primer elemento:',self.ymin,self.ymax)
				else:
					#print(type(self.Object.ymin))
					if(self.Object.ymin<self.ymin):
						self.ymin=self.Object.ymin
						#print('Minimo actualizado:',self.ymin)
					elif(self.Object.ymax>self.ymax):
						
						self.ymax=self.Object.ymax
						#print('Maximo actualizado:',self.ymax)

