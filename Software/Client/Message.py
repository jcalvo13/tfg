from Image import *
from svgpathtools import parse_path


#RANGO_INTENSIDAD=255
#Intensidad de negro en función de la separación de líneas, no del numero de lineas 
#(el número de lineas depender,a de los grande que sea el path)
MIN_ESPACIO_LINEAS=3  #0.8
MAX_ESPACIO_LINEAS=10 #para ordenador 2-3

ANCHO_IMAGEN_REAL=714 #DImensiones de la pizarra
ALTO_IMAGEN_REAL=714

AMPLITUD=5.0

class Message:
	def __init__(self,order,values):
		self.order=order
		self.values=values

	def Print(self):
		print(self.order,self.values)
	def Serialize(self): #Los parámetros se consideran float, falta mulitplicarlos por el factor del viewport
		order=self.order.to_bytes(1,'little')
		buf = bytes()
		while(len(self.values)<=12):  #rellenamos el mensaje con 0 para que todos los mensajes ocupen lo mismo
			self.values.insert(len(self.values),0.0)
		print(self.values)
		for val in self.values:
			buf += struct.pack('f', val)
		
		
		return (order+buf)




class MessageList: #Podría ser llamado interfaz
	def __init__(self,imagen):
		self.x,self.y=imagen.getViewPort()
		self.factorx,self.factory=ANCHO_IMAGEN_REAL/self.x,ALTO_IMAGEN_REAL/self.y  #1,1
		self.alto=None
		self.list=[] #lista de mensajes
		self.listbin=[] #lista de mensajes serializados, para pasar al socket
		self.values=[]
		self.path2paint=[] #lista de paths para comprobacion del relleno SVGWRITE
	def path2message(self,path): #le paso la LISTA DE ELEMENTOS de solo un path, no la lista   para cada path llamo a esta funcion
		for i in range(0,len(path.elementList)):
			if(type(path.elementList[i])==type(MoveAbs())):
				order=1				
				self.values.append(path.elementList[i].fin.x*self.factorx)
				self.values.append(path.elementList[i].fin.y*self.factory)
				#MODIFICANDO

			elif(type(path.elementList[i])==type(MoveRel())):
				order=2
				self.values.append(path.elementList[i].fin.x*self.factorx)
				self.values.append(path.elementList[i].fin.y*self.factory)

			elif(type(path.elementList[i])==type(LineAbs())):
				order=3
				self.values.append(path.elementList[i].fin.x*self.factorx)
				self.values.append(path.elementList[i].fin.y*self.factory)


			elif(type(path.elementList[i])==type(LineRel())):
				order=4
				self.values.append(path.elementList[i].fin.x*self.factorx)
				self.values.append(path.elementList[i].fin.y*self.factory)

			elif(type(path.elementList[i])==type(CuaAbs())):
				order=5
				self.values.append(path.elementList[i].c1.x*self.factorx)
				self.values.append(path.elementList[i].c1.y*self.factory)
				self.values.append(path.elementList[i].fin.x*self.factorx)
				self.values.append(path.elementList[i].fin.y*self.factory)

			elif(type(path.elementList[i])==type(CuaRel())):
				order=6
				self.values.append(path.elementList[i].c1.x*self.factorx)
				self.values.append(path.elementList[i].c1.y*self.factory)
				self.values.append(path.elementList[i].fin.x*self.factorx)
				self.values.append(path.elementList[i].fin.y*self.factory)

			elif(type(path.elementList[i])==type(CubAbs())):
				order=7
				self.values.append(path.elementList[i].c1.x*self.factorx)
				self.values.append(path.elementList[i].c1.y*self.factory)
				self.values.append(path.elementList[i].c2.x*self.factorx)
				self.values.append(path.elementList[i].c2.y*self.factory)
				self.values.append(path.elementList[i].fin.x*self.factorx)
				self.values.append(path.elementList[i].fin.y*self.factory)

			elif(type(path.elementList[i])==type(CubRel())):
				order=8
				a=path.elementList[i].ini.x*self.factorx
				b=path.elementList[i].ini.y*self.factory
				c=path.elementList[i].c1.x*self.factorx
				d=path.elementList[i].c1.y*self.factory
				e=path.elementList[i].c2.x*self.factorx
				f=path.elementList[i].c2.y*self.factory
				g=path.elementList[i].fin.x*self.factorx
				h=path.elementList[i].fin.y*self.factory

				self.values.append(c)
				self.values.append(d)
				self.values.append(e)
				self.values.append(f)
				self.values.append(g)
				self.values.append(h)
				#print('Control point')
				self.path2paint.append(CubicBezier(a+b*1j,(a+c)+(b+d)*1j,(a+e)+(b+f)*1j,(a+g)+(b+h)*1j))


			message=Message(order,self.values)
			self.list.append(message)
			self.listbin.append(message.Serialize())
			self.values=[]

	#ADMINISTRACION DEL COLOR

	def fill2sin(self,path): #llamar antes a solve y luego llamar al min y max de path

		intensidad=path.color2gray()

		intensidad=255-intensidad
		intensidad=(intensidad/255)*20   #*35 para rotus
		#print(type(intensidad))
		#print(type(path.ymax),type(path.ymin))
		#self.alto=path.ymax-path.ymin
		espacio=2*AMPLITUD*self.factory

		#print(intensidad,espacio)

		#num_lineas= self.alto/espacio

		y=path.ymin

		while(y<=(path.ymax-espacio)): #restamos espacio porque no queremos que sobrepase la ymax
			lista=[]
			y+=espacio
			for i in range(0,len(path.elementList)):

				
				if(path.elementList[i].Solve(y)!=None):
					#print('Corte de y= ',y,'con elemento: ',j)
					x=path.elementList[i].Solve(y)

					#path.elementList[i].Print()
					#print('Y:',y,'con x= ',x)
					for i in range(0,len(x)):
						x[i]=x[i]*self.factorx
					#x=x[0]*self.factorx
					
					#x=sorted(x)
					#imagen1.pathlist[0].elementList[j].Show()
					#print('Corte del elemento numero: ',j,'con y=',y,'Solución: ',x)
						lista.append(x[i])

				lista=sorted(lista)
			#print('La lista',lista)
			#coger los valores de 2 en 2 e ir rellenando el mensaje (orden,amplitud,intensidad,y,x0,x1)
			i=0
			values=[]
			while(i<len(lista)-1):
				
				values.append(AMPLITUD)
				values.append(intensidad)
				values.append(y)
				values.append(lista[i])
				values.append(lista[i+1])
				message=Message(11,values)
				self.list.append(message)
				self.listbin.append(message.Serialize())
				values=[]
				i+=2

	def fill2sincolor(self,path): #llamar antes a solve y luego llamar al min y max de path

		intensidad=path.color2CMYK()
		herramienta=(4,1,2,3)

		espacio=2*AMPLITUD


		for j in range(0,len(intensidad)):

			intens=intensidad[j]


			if intens>0.05 and intens<0.3:
				intens=intens*5

			elif intens>=0.3 and intens<0.8:
				intens=intens*10

			else: 
				intens=intens*20
			#intens=255-intens
			#intens=intens*15  #*35 para rotus




			#print(intens)

			#Agregar funcion saturacion intensidad

			if intens>0.05:

				print('****INTENSIDAD DE CAPA*****',intens,'herramienta: ',herramienta[j])
				#mensaje de cambio de herramienta
				lista=[]
				lista.append(herramienta[j])
				message=Message(21,lista)
				self.list.append(message)
				self.listbin.append(message.Serialize())
				values=[]
				lista=[]


				y=path.ymin

				while(y<=(path.ymax-espacio)): #restamos espacio porque no queremos que sobrepase la ymax
					lista=[]
					y+=espacio
					for i in range(0,len(path.elementList)):

						
						if(path.elementList[i].Solve(y)!=None):
							#print('Corte de y= ',y,'con elemento: ',j)
							x=path.elementList[i].Solve(y)

							#path.elementList[i].Print()
							#print('Y:',y,'con x= ',x)
							for i in range(0,len(x)):
								x[i]=x[i]*self.factorx
							#x=x[0]*self.factorx
							
							#x=sorted(x)
							#imagen1.pathlist[0].elementList[j].Show()
							#print('Corte del elemento numero: ',j,'con y=',y,'Solución: ',x)
								lista.append(x[i])

						lista=sorted(lista)
					#print('La lista',lista)
					#coger los valores de 2 en 2 e ir rellenando el mensaje (orden,amplitud,intensidad,y,x0,x1)
					i=0
					values=[]
					while(i<len(lista)-1):
						
						values.append(AMPLITUD*self.factory)
						values.append(intens)
						values.append((y-espacio)*self.factory+espacio)
						values.append(lista[i])
						values.append(lista[i+1])
						message=Message(11,values)
						self.list.append(message)
						self.listbin.append(message.Serialize())
						values=[]
						i+=2






	def Print(self):
		for i in range(0,len(self.list)):
			self.list[i].Print()
			print(self.listbin[i])



	def Paint(self,lista):
		#print(len(lista),lista)
		#print(type(lista[2]))
		i=0
		while i<(len(lista)-2):
			#print((lista[i+2]-lista[i+1]))
			#if((lista[i+2]-lista[i+1])>15): #filter to don't paint the shape
			#print('control point')
			self.PaintLine(lista[0],lista[i+1],lista[i+2])
			i+=2

		
	def PaintLine(self,y,x1,x2):
 		self.path2paint.append(Line(x1+y*1j,x2+y*1j))

	def fill2messagescolor(self,path): #llamar antes a solve y luego llamar al min y max de path
		print('Funcion messages color')
		intensidad=path.color2CMYK()
		herramienta=(1,2,3,4)
		#print(type(intensidad))
		#print(type(path.ymax),type(path.ymin))
		#self.alto=path.ymax-path.ymin
		for j in range(0,len(intensidad)):




			#espacio=((MAX_ESPACIO_LINEAS-MIN_ESPACIO_LINEAS)/(1-0))*intensidad[j]+MIN_ESPACIO_LINEAS
			espacio=MAX_ESPACIO_LINEAS+((MAX_ESPACIO_LINEAS-MIN_ESPACIO_LINEAS)/(0-1))*intensidad[j]

			y=path.ymin

			print('Intensidad de capa',intensidad[j],'herramienta: ',herramienta[j],'Espacio: ',espacio)
			#mensaje de cambio de herramienta
			lista=[]
			lista.append(herramienta[j])
			message=Message(11,lista)
			self.list.append(message)
			self.listbin.append(message.Serialize())
			values=[]



			while(y<=(path.ymax-espacio)): #restamos espacio porque no queremos que sobrepase la ymax
				lista=[]
				y+=espacio
				for i in range(0,len(path.elementList)):

					
					if(path.elementList[i].Solve(y)!=None):
						#print('Corte de y= ',y,'con elemento: ',j)
						x=path.elementList[i].Solve(y)

						#path.elementList[i].Print()
						#print('Y:',y,'con x= ',x)
						for i in range(0,len(x)):
							x[i]=x[i]*self.factorx
						#x=x[0]*self.factorx
						
						#x=sorted(x)
						#imagen1.pathlist[0].elementList[j].Show()
						#print('Corte del elemento numero: ',j,'con y=',y,'Solución: ',x)
							lista.append(x[i])
				if(len(self.list)%2==0):
					lista=sorted(lista)
				else:
					lista=sorted(lista,reverse=True)
				lista.insert(0, y*self.factory)
				#print(lista)
				self.Paint(lista)#AGREGAR FUNCION
				"""while(len(lista)<9):  #rellenamos el mensaje con 0 para que todos los mensajes ocupen lo mismo
					lista.insert(len(lista),0.0)
				"""

				message=Message(10,lista)
				self.list.append(message)
				self.listbin.append(message.Serialize())
				self.values=[]


	def fill2messages(self,path): #llamar antes a solve y luego llamar al min y max de path

		intensidad=path.color2gray()
		#print(type(intensidad))
		#print(type(path.ymax),type(path.ymin))
		#self.alto=path.ymax-path.ymin
		espacio=((MAX_ESPACIO_LINEAS-MIN_ESPACIO_LINEAS)/(255-0))*intensidad+MIN_ESPACIO_LINEAS

		#print(intensidad,espacio)

		#num_lineas= self.alto/espacio

		y=path.ymin

		while(y<=(path.ymax-espacio)): #restamos espacio porque no queremos que sobrepase la ymax
			lista=[]
			y+=espacio
			for i in range(0,len(path.elementList)):

				
				if(path.elementList[i].Solve(y)!=None):
					#print('Corte de y= ',y,'con elemento: ',j)
					x=path.elementList[i].Solve(y)

					#path.elementList[i].Print()
					#print('Y:',y,'con x= ',x)
					for i in range(0,len(x)):
						x[i]=x[i]*self.factorx
					#x=x[0]*self.factorx
					
					#x=sorted(x)
					#imagen1.pathlist[0].elementList[j].Show()
					#print('Corte del elemento numero: ',j,'con y=',y,'Solución: ',x)
						lista.append(x[i])
			if(len(self.list)%2==0):
				lista=sorted(lista)
			else:
				lista=sorted(lista,reverse=True)
			lista.insert(0, y*self.factory)
			#print(lista)
			self.Paint(lista)#AGREGAR FUNCION
			"""while(len(lista)<9):  #rellenamos el mensaje con 0 para que todos los mensajes ocupen lo mismo
				lista.insert(len(lista),0.0)
			"""

			message=Message(10,lista)
			self.list.append(message)
			self.listbin.append(message.Serialize())
			self.values=[]
				#self.list.append(lista)

